#!/usr/bin/env node

/*
 * NOTE: This script provides an alternative solution to ImageMagick
 * but it is not used at this moment. 
 */

var canvg = require('canvg'),
	Canvas = require('canvas');


var svg_f = process.argv[2],
	width = process.argv[3],
	height = process.argv[4];

console.log(svg_f, width, height);

var fs = require('fs');

var svg = fs.readFileSync(svg_f, 'utf-8');

var canvas = new Canvas(),
    ctx = canvas.getContext("2d");

canvas.width = width * 2;
canvas.height = height * 2;


canvg(canvas, svg, 0, 0, width * 2, height * 2);


// document.body.appendChild(canvas);

// var imgData = canvas.toDataURL("image/png");

var out = fs.createWriteStream(__dirname + '/../test.png'),
	stream = canvas.pngStream();

stream.on('data', function(chunk){
  	out.write(chunk);
});

stream.on('end', function(){
  	console.log('saved png');
});

