<?php

DatawrapperHooks::register(DatawrapperHooks::PROVIDE_API, function() use ($plugin) {

    global $app;

    return array(
        'url' => 'snapshot',
        'method' => 'POST',
        'action' => function() use ($app, $plugin) {

            $body   = json_decode($app->request()->getBody(), true);
            $config = $plugin->getConfig();
            $magic  = isset($config['imagemagick_binary']) ? $config['imagemagick_binary'] : 'convert';

            if_chart_is_writable($body['chart_id'], function($user, $chart) use ($body, $magic) {
                $fn_svg = tempnam('dw', 'snapshot').'.svg';
                $fn_png = get_static_path($chart) . '/' . $body['thumb_id'] . '.png';

                file_put_contents($fn_svg, $body['svg']);

                $stdout = []; $stderr = [];

                @exec($magic.' -density 144 '.$fn_svg.' '.$fn_png, $stdout, $stderr);

                print json_encode([
                    'error' => 0,
                    'stdout' => $stdout,
                    'stderr' => $stderr,
                    'res' => 'ok'
                ]);
            });

        }
    );
});
